Testing an algorithm on the training data gives deceptively good results.  To have
separate test data, I have set aside about one third of Kaggle's training data.  
This results in two data files:

* mytrain.csv - 599 records, used for training algorithms
* mytest.csv - 292 records, used for testing algorithms

Code is in learn.py .  This code is *not* set up to return a predictions dictionary
for assessment by Udacity's system.  Instead it runs a report() method to print the
percentage of successful predictions.

There is one object in the code, called a Titanic.
It has attributes:
.train - the training data, as a pandas DataFrame
.test - the test data, as a pandas DataFrame
.pred_out - dictionary whose keys are passenger_index numbers and whose values 
are (prediction, outcome) 2-tuples.

Results at this stage are:
* Women and Upper Class Children First - 64.7% success rate.
* Binning approach, reverting to the Women and Upper Class Children First (WAUCC) 
method for test data that maps to a bin that wasn't in the training data - 64.7% 
success rate (the binning approach differed from the WAUCC method on 24 test cases and 
was better in exactly half of them).
* Least squares approach, based on code from
http://glowingpython.blogspot.com.au/2012/03/linear-regression-with-numpy.html - 72.9% 
success rate.

