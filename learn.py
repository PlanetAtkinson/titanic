import numpy
import pandas
import statsmodels.api as sm

class Titanic:
    def __init__(self):
        self.pred_out = {}
        # key is passenger_id for passenger in the test data
        # value is (prediction, outcome)
        
    def encode_passenger(self, passenger):
        age = passenger['Age']
        if age == age:
            age_bin = int(age/10.)
            age_nan = 0
        else:
            age_bin = 9
            age_nan = 1
        name = passenger['Name']
        name_len = len(name.split(' '))
        sibsp = passenger['SibSp']
        parch = passenger['Parch']
        ticket = passenger['Ticket']
        if any([t.isalpha() for t in ticket]):
            alpha_ticket = 1
        else:
            alpha_ticket = 0
        fare = passenger['Fare']
        if fare == fare:
            fare_bin = fare/60.
            fare_nan = 0
        else:
            fare_bin == 9
            fare_nan = 1
        return (age_bin, age_nan, name_len, sibsp, parch, 
                alpha_ticket, fare_bin, fare_nan)
        
    def train_binned(self, train_fname):
        self.train = pandas.read_csv(train_fname)
        self.training = {}
        # key is passenger encoding list, value is (n_survivors, n_perishers) for that bin
        for passenger_index, passenger in self.train.iterrows():
            code = self.encode_passenger(passenger)
            if code not in self.training:
                self.training[code] = [0, 0]
            if passenger['Survived'] == 1:
                self.training[code][0] += 1
            else:
                self.training[code][1] += 1
                
    def train_linear(self, train_fname):
        # based on code at
        # http://glowingpython.blogspot.com.au/2012/03/linear-regression-with-numpy.html
        self.train = pandas.read_csv(train_fname)
        n_cases, humbug = self.train.shape
        n_fields = 8 # length of rvalue from encode_passenger()
        A = numpy.ones((n_fields + 1) * n_cases).reshape(n_cases, n_fields + 1)
        for passenger_index, passenger in self.train.iterrows():
            code = self.encode_passenger(passenger)
            A[passenger_index, :] = list(code) + [1]
        y = numpy.array(self.train['Survived'])
        self.w = numpy.linalg.lstsq(A,y)[0]
        
    def test_linear(self, test_fname):
        self.pred_out = {}
        self.test = pandas.read_csv(test_fname)
        for passenger_index, passenger, in self.test.iterrows():
            code = self.encode_passenger(passenger)
            x = list(code) + [1]
            yhat = sum([self.w[i] * x[i] for i in range(len(self.w))]) # dot product
            # yhat is values predicted as function of x-vector by the linear model
            if yhat > 0.5:
                prediction = 1
            else:
                prediction = 0
            self.pred_out[passenger_index] = [prediction, passenger['Survived']]            
            
    def test_binned(self, test_fname):
        self.test = pandas.read_csv(test_fname)
        for passenger_index, passenger in self.test.iterrows():
            outcome = passenger['Survived']
            code = self.encode_passenger(passenger)
            if code not in self.training:
                # if passenger's bin did not appear in training data, go with WAUCC
                if (passenger['Sex'] == 'F' or (passenger['Age'] < 18 and 
                    passenger['Pclass'] == 1)):
                    prediction = 1
                else:
                    prediction = 0      
            else:
                bin_survivors, bin_perishers = self.training[code]
                if bin_survivors > bin_perishers:
                    prediction = 1
                else:
                    prediction = 0
            self.pred_out[passenger_index] = (prediction, int(outcome))
        
    def test_WAUCC(self, test_fname):
        # women and upper class children first
        self.test = pandas.read_csv(test_fname)
        for passenger_index, passenger in self.test.iterrows():
            if (passenger['Sex'] == 'F' or (passenger['Age'] < 18 and 
                    passenger['Pclass'] == 1)):
                prediction = 1
            else:
                prediction = 0      
            outcome = passenger['Survived']
            self.pred_out[passenger_index] = (prediction, int(outcome))
            
    def report(self):
        successes = 0
        failures = 0
        for prediction, outcome in self.pred_out.values():
            if prediction == outcome:
                successes +=1
            else:
                failures += 1
        print successes, 'successes and', failures, 'failures:',
        print (successes * 100.0)/(successes + failures), 'per cent'
        print

if __name__ == '__main__':
    t = Titanic()
    print 'Women and upper class children first'
    # WAUCC gets a base rate of 64.7%
    t.test_WAUCC('mytest.csv')
    t.report()
    print 'WAUCC overridden by binning'
    # Gets exactly the same result on my test data as WAUCC, this is coincidence
    t.train_binned('mytrain.csv')
    t.test_binned('mytest.csv')
    t.report()
    print 'linear algebra'
    # linear algebra does better with 72.9%
    t.train_linear('mytrain.csv')
    t.test_linear('mytest.csv')
    t.report()

